flask
redis
Flask-SQLAlchemy
psycopg2-binary
werkzeug>=3.0.1 # not directly required, pinned by Snyk to avoid a vulnerability